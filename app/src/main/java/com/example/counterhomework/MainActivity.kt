package com.example.counterhomework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    var mCount: Int = 0
    lateinit var count: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        count = findViewById(R.id.countView)
        if (savedInstanceState != null) {
            count.text = savedInstanceState.getInt("count").toString()
            mCount = savedInstanceState.getInt("count")
        }
    }

    fun countUp(view: View) {
        mCount++
        count.text = mCount.toString()
    }

    override fun onSaveInstanceState(myState: Bundle) {
        super.onSaveInstanceState(myState)
        myState.putInt("count", mCount)
    }
}